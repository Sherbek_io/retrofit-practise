package uz.optimal.retrofitpractise.cache.source

import uz.optimal.retrofitpractise.cache.model.EntityPhoto

interface PhotoLocalDaSource {
    fun getAll(): List<EntityPhoto>
    suspend fun insert(photo: EntityPhoto)

    suspend fun insertAll(list: List<EntityPhoto>)
}