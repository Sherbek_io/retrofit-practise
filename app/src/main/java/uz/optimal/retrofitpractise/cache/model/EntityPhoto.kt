package uz.optimal.retrofitpractise.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photo_db")
class EntityPhoto(

    @PrimaryKey
    val id: String,
    val author: String,
    val width: Int,
    val downloadUrl: String,
    val url: String,
    val height: Int

)