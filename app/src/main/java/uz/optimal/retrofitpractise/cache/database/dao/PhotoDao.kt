package uz.optimal.retrofitpractise.cache.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uz.optimal.retrofitpractise.cache.model.EntityPhoto

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: EntityPhoto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<EntityPhoto>)

    @Query("select * from photo_db")
    fun getAll(): List<EntityPhoto>

}