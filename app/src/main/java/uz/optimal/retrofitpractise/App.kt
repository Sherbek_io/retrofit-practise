package uz.optimal.retrofitpractise

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App:MultiDexApplication()