package uz.optimal.retrofitpractise.network.data.source.abstraction

import uz.optimal.retrofitpractise.network.data.model.DomainPhoto

interface PhotoNetworkDataSource {

    suspend fun getPhotos():List<DomainPhoto>
}