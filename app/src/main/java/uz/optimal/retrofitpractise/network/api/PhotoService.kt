package uz.optimal.retrofitpractise.network.api

import retrofit2.http.GET
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto

interface PhotoService {

    @GET("v2/list")
    fun getAllPhoto(): List<DomainPhoto>

}