package uz.optimal.retrofitpractise.common.mapper

interface UIMapper<UIModel, DomainModel> {

    fun mapFromUI(uiModel: UIModel): DomainModel

    fun mapToUI(domainModel: DomainModel): UIModel

    open fun mapToUIList(list: List<DomainModel>):List<UIModel>{
        return ArrayList<UIModel>().apply {
            list.forEach{
                add(mapToUI(it))
            }
        }
    }

}