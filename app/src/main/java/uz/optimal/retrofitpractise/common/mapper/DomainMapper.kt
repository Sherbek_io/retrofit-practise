package uz.optimal.retrofitpractise.common.mapper

interface DomainMapper<DomainModel, Entity> {
    fun mapFromNetwork(domainModel: DomainModel): Entity

    fun mapToNetwork(entity: Entity): DomainModel

    open fun mapFromNetworkList(list: List<DomainModel>):List<Entity>{
        return ArrayList<Entity>().apply {
            list.forEach{
                add(mapFromNetwork(it))
            }
        }
    }

}