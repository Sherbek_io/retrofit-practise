package uz.optimal.retrofitpractise.common


// Created by Anaskhan on 6/21/2021.

sealed class UIState<T> {
    class Success<T>(val data:T? = null): UIState<T>()

}