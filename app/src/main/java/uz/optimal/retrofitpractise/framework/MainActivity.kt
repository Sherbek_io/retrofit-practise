package uz.optimal.retrofitpractise.framework

import android.app.Application
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp
import kotlinx.android.synthetic.main.activity_main.*
import uz.optimal.retrofitpractise.R
import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.common.UIState

@HiltAndroidApp
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

}