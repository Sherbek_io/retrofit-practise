package uz.optimal.retrofitpractise.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uz.optimal.retrofitpractise.network.data.source.abstraction.PhotoNetworkDataSource
import uz.optimal.retrofitpractise.network.data.source.implementation.PhotoNetworkDataSourceImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkDataSourceModule {

    @Singleton
    @Provides
    fun provideTradeDataSource(photoNetworkDataSourceImpl: PhotoNetworkDataSourceImpl):PhotoNetworkDataSource =
        photoNetworkDataSourceImpl
}