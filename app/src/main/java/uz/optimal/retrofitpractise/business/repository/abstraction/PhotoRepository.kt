package uz.optimal.retrofitpractise.business.repository.abstraction

import kotlinx.coroutines.flow.Flow
import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.common.UIState
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto

interface PhotoRepository {

     fun getPhotos(): Flow<UIState<List<UIPhoto>>>

}