package uz.optimal.retrofitpractise.business.mapper

import uz.optimal.retrofitpractise.cache.model.EntityPhoto
import uz.optimal.retrofitpractise.common.mapper.DomainMapper
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto
import javax.inject.Inject

class PhotoDomainMapper @Inject constructor() :
    DomainMapper<DomainPhoto, EntityPhoto> {
    override fun mapFromNetwork(domainModel: DomainPhoto): EntityPhoto {
        return EntityPhoto(
            domainModel.id,
            domainModel.author,
            domainModel.width,
            domainModel.downloadUrl,
            domainModel.url,
            domainModel.height
        )
    }

    override fun mapToNetwork(entity: EntityPhoto): DomainPhoto {
        TODO("Not yet implemented")
    }

}