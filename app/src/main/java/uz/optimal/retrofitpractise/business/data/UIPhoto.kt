package uz.optimal.retrofitpractise.business.data

class UIPhoto (
    val id: String,
    val author: String,
    val width: Int,
    val downloadUrl: String,
    val url: String,
    val height: Int
)